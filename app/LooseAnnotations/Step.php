<?php

namespace App\LooseAnnotations;

/**
 * Class Step
 * @package App
 *
 * @OA\Tag(
 *   name="Step",
 *   description="Step Operations"
 *
 * )
 * @OA\Schema(
 *     title="Step",
 *     description="Step model",
 *     required={"id","key","value"},
 *     @OA\Xml(
 *         name="Step"
 *     )
 * )
 */



class Step
{
  /**
       * @OA\Property(
       *     property="id",
       *     title="Step ID",
       *     description="Step Identity",
       *     type="int",
       *     example=1
       * )
       *
       * @OA\Property(
       *     property="key",
       *     title="Step key",
       *     description="Step status",
       *     type="string",
       *     example="contactado"
       * )
       *
       * @OA\Property(
       *     property="value",
       *     title="Step value",
       *     description="Step value status",
       *     type="string",
       *     example="contactado"
       * )
       */
}
