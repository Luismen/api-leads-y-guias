<?php

namespace App\LooseAnnotations;


/**
 * @OA\Get(path="/api/leads/{id}",
 *   tags={"Lead"},
 *   summary="Show",
 *   description="Returns lead by id",
 *   @OA\Parameter(
 *     name="id",
 *     in="path",
 *     description="Lead ID",
 *     required=true,
 *     @OA\Schema(
 *         type="string"
 *     )
 *   ),
 *   @OA\Response(
 *         response=200,
 *         description="Successful operation.",
 *         @OA\JsonContent(
 *              @OA\Property(
 *                  property="message",
 *                  example="Resource Found"
 *             ),
 *              @OA\Property(
 *                  property="data",
 *                  ref="#/components/schemas/Lead"
 *             ),
 *
 *          ),
 *     ),
 *     @OA\Response(
 *         response="401",
 *         description="Unauthenticated."
 *     ),
 *     @OA\Response(
 *         response="404",
 *         description="Not Found."
 *     ),
 *     @OA\Response(
 *         response="500",
 *         description="CRM failure."
 *     )
 * )
 */

/**
 * @OA\Post(path="/api/leads",
 *   tags={"Lead"},
 *   summary="Store",
 *   description="Store Lead",
 *   @OA\RequestBody(
 *       required=true,
 *       description="Lead information",
 *       @OA\JsonContent(ref="#/components/schemas/Lead")
 *   ),
 *   @OA\Response(
 *         response=200,
 *         description="Successful operation.",
 *         @OA\JsonContent(
 *              @OA\Property(
 *                  property="message",
 *                  example="Resource Store Success"
 *             ),
 *              @OA\Property(
 *                  property="data",
 *                  ref="#/components/schemas/Lead"
 *             ),
 *
 *          ),
 *     ),
 *     @OA\Response(
 *         response="401",
 *         description="Unauthenticated."
 *     ),
 *     @OA\Response(
 *         response="404",
 *         description="Not Found."
 *     ),
 *     @OA\Response(
 *         response="500",
 *         description="CRM failure."
 *     )
 * )
 */


/**
 * @OA\Put(path="/api/leads/{id}",
 *   tags={"Lead"},
 *   summary="Update",
 *   description="Update Lead",
 *   @OA\RequestBody(
 *       required=true,
 *       description="Lead information",
 *       @OA\JsonContent(ref="#/components/schemas/Lead")
 *   ),
 *   @OA\Parameter(
 *     name="id",
 *     in="path",
 *     description="Lead ID",
 *     required=true,
 *     @OA\Schema(
 *         type="string"
 *     )
 *   ),
 *   @OA\Response(
 *         response=200,
 *         description="Successful operation.",
 *         @OA\JsonContent(
 *              @OA\Property(
 *                  property="message",
 *                  example="Updated"
 *             ),
 *              @OA\Property(
 *                  property="data",
 *                  ref="#/components/schemas/Lead"
 *             ),
 *
 *          ),
 *     ),
 *     @OA\Response(
 *         response="401",
 *         description="Unauthenticated."
 *     ),
 *     @OA\Response(
 *         response="404",
 *         description="Not Found."
 *     ),
 *     @OA\Response(
 *         response="500",
 *         description="CRM failure."
 *     )
 * )
 */

/**
 * @OA\Delete(path="/api/leads/{id}",
 *   tags={"Lead"},
 *   summary="Delete",
 *   description="Delete Lead",
 *
 *   @OA\Parameter(
 *     name="id",
 *     in="path",
 *     description="Lead ID",
 *     required=true,
 *     @OA\Schema(
 *         type="string"
 *     )
 *   ),
 *   @OA\Response(
 *         response=200,
 *         description="Successful operation.",
 *         @OA\JsonContent(
 *              @OA\Property(
 *                  property="message",
 *                  example="Resource Deleted"
 *             ),
 *              @OA\Property(
 *                  property="data",
 *                  ref="#/components/schemas/Lead"
 *             ),
 *
 *          ),
 *     ),
 *     @OA\Response(
 *         response="401",
 *         description="Unauthenticated."
 *     ),
 *     @OA\Response(
 *         response="404",
 *         description="Not Found."
 *     ),
 *     @OA\Response(
 *         response="500",
 *         description="CRM failure."
 *     )
 * )
 */
