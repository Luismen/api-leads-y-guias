<?php

namespace App\LooseAnnotations;


/**
 * @OA\Get(path="/api/steps/{id}",
 *   tags={"Step"},
 *   summary="Show",
 *   description="Returns Step by id",
 *   @OA\Parameter(
 *     name="id",
 *     in="path",
 *     description="Step ID",
 *     required=true,
 *     @OA\Schema(
 *         type="string"
 *     )
 *   ),
 *   @OA\Response(
 *         response=200,
 *         description="Successful operation.",
 *         @OA\JsonContent(
 *              @OA\Property(
 *                  property="message",
 *                  example="Resource Found"
 *             ),
 *              @OA\Property(
 *                  property="data",
 *                  ref="#/components/schemas/Step"
 *             ),
 *
 *          ),
 *     ),
 *     @OA\Response(
 *         response="401",
 *         description="Unauthenticated."
 *     ),
 *     @OA\Response(
 *         response="404",
 *         description="Not Found."
 *     ),
 *     @OA\Response(
 *         response="500",
 *         description="CRM failure."
 *     )
 * )
 */

/**
 * @OA\Post(path="/api/steps",
 *   tags={"Step"},
 *   summary="Store",
 *   description="Store Step",
 *   @OA\RequestBody(
 *       required=true,
 *       description="Step information",
 *       @OA\JsonContent(ref="#/components/schemas/Step")
 *   ),
 *   @OA\Response(
 *         response=200,
 *         description="Successful operation.",
 *         @OA\JsonContent(
 *              @OA\Property(
 *                  property="message",
 *                  example="Resource Store Success"
 *             ),
 *              @OA\Property(
 *                  property="data",
 *                  ref="#/components/schemas/Step"
 *             ),
 *
 *          ),
 *     ),
 *     @OA\Response(
 *         response="401",
 *         description="Unauthenticated."
 *     ),
 *     @OA\Response(
 *         response="404",
 *         description="Not Found."
 *     ),
 *     @OA\Response(
 *         response="500",
 *         description="CRM failure."
 *     )
 * )
 */


/**
 * @OA\Put(path="/api/steps/{id}",
 *   tags={"Step"},
 *   summary="Update",
 *   description="Update Step",
 *   @OA\RequestBody(
 *       required=true,
 *       description="Lead information",
 *       @OA\JsonContent(ref="#/components/schemas/Step")
 *   ),
 *   @OA\Parameter(
 *     name="id",
 *     in="path",
 *     description="Step ID",
 *     required=true,
 *     @OA\Schema(
 *         type="string"
 *     )
 *   ),
 *   @OA\Response(
 *         response=200,
 *         description="Successful operation.",
 *         @OA\JsonContent(
 *              @OA\Property(
 *                  property="message",
 *                  example="Updated"
 *             ),
 *              @OA\Property(
 *                  property="data",
 *                  ref="#/components/schemas/Step"
 *             ),
 *
 *          ),
 *     ),
 *     @OA\Response(
 *         response="401",
 *         description="Unauthenticated."
 *     ),
 *     @OA\Response(
 *         response="404",
 *         description="Not Found."
 *     ),
 *     @OA\Response(
 *         response="500",
 *         description="CRM failure."
 *     )
 * )
 */

/**
 * @OA\Delete(path="/api/steps/{id}",
 *   tags={"Step"},
 *   summary="Delete",
 *   description="Delete Step",
 *
 *   @OA\Parameter(
 *     name="id",
 *     in="path",
 *     description="Step ID",
 *     required=true,
 *     @OA\Schema(
 *         type="string"
 *     )
 *   ),
 *   @OA\Response(
 *         response=200,
 *         description="Successful operation.",
 *         @OA\JsonContent(
 *              @OA\Property(
 *                  property="message",
 *                  example="Resource Deleted"
 *             ),
 *              @OA\Property(
 *                  property="data",
 *                  ref="#/components/schemas/Step"
 *             ),
 *
 *          ),
 *     ),
 *     @OA\Response(
 *         response="401",
 *         description="Unauthenticated."
 *     ),
 *     @OA\Response(
 *         response="404",
 *         description="Not Found."
 *     ),
 *     @OA\Response(
 *         response="500",
 *         description="CRM failure."
 *     )
 * )
 */
